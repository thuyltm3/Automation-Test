$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/automation/tiki.feature");
formatter.feature({
  "name": "",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Search and buy an item on Tiki.vn",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Go to Tiki.vn",
  "keyword": "Given "
});
formatter.match({
  "location": "automation.tiki.TikiStepdefs.go_to_tikivn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Search item",
  "rows": [
    {},
    {}
  ],
  "keyword": "When "
});
formatter.match({
  "location": "automation.tiki.TikiStepdefs.search(java.util.List\u003cjava.lang.String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Visit item detail page",
  "keyword": "And "
});
formatter.match({
  "location": "automation.tiki.TikiStepdefs.visit_item_detail_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Add item to cart",
  "keyword": "And "
});
formatter.match({
  "location": "automation.tiki.TikiStepdefs.add_items_to_cart()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Visit to Cart page",
  "keyword": "And "
});
formatter.match({
  "location": "automation.tiki.TikiStepdefs.visit_to_cart_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Process the order",
  "keyword": "And "
});
formatter.match({
  "location": "automation.tiki.TikiStepdefs.process_the_order()"
});
formatter.result({
  "error_message": "org.openqa.selenium.TimeoutException: Expected condition failed: waiting for visibility of element located by By.cssSelector: button[class\u003d\u0027cart__submit\u0027] (tried for 15 second(s) with 500 milliseconds interval)\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027stack-pc\u0027, ip: \u0027192.168.6.66\u0027, os.name: \u0027Linux\u0027, os.arch: \u0027amd64\u0027, os.version: \u00275.3.0-29-generic\u0027, java.version: \u002713.0.2\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 78.0.3904.108, chrome: {chromedriverVersion: 78.0.3904.105 (60e2d8774a81..., userDataDir: /tmp/.com.google.Chrome.YO1e7h}, goog:chromeOptions: {debuggerAddress: localhost:36385}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: LINUX, platformName: LINUX, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify}\nSession ID: 1bfd39bd7291d93c5f7ac653eedd70a6\n\tat org.openqa.selenium.support.ui.WebDriverWait.timeoutException(WebDriverWait.java:95)\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:272)\n\tat automation.tiki.TikiStepdefs.process_the_order(TikiStepdefs.java:113)\n\tat ✽.Process the order(file:///home/thangnm/Downloads/Automation/Automation/src/test/resources/automation/tiki.feature:11)\n",
  "status": "failed"
});
formatter.step({
  "name": "Do login if Login Screen showed",
  "keyword": "And "
});
formatter.match({
  "location": "automation.tiki.TikiStepdefs.login_with_if_login_screen_showed()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Select Shipping address",
  "keyword": "And "
});
formatter.match({
  "location": "automation.tiki.TikiStepdefs.select_shipping_address()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Order",
  "keyword": "And "
});
formatter.match({
  "location": "automation.tiki.TikiStepdefs.order()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "show order number",
  "keyword": "Then "
});
formatter.match({
  "location": "automation.tiki.TikiStepdefs.show_order_number()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
});