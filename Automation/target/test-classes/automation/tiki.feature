Feature:
  Scenario: Search and buy an item on Tiki.vn
    Given Go to Tiki.vn
    When Search item
#      | Quạt Kẹp Mini  |
      | khẩu trang  |
      | Giấy Ăn Gấu Trúc  |
    And Visit item detail page
    And Add item to cart
    And Visit to Cart page
    And Process the order
    And Do login if Login Screen showed
    And Select Shipping address
    And Order
    Then show order number